package com.gitlab.edersix.security.services;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author edersix.
 */
@Repository
public interface GuestRepository extends JpaRepository<Guest, Long> {
}
