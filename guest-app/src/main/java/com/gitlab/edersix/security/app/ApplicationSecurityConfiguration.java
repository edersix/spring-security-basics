package com.gitlab.edersix.security.app;

//import java.util.ArrayList;
//import java.util.List;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.provisioning.InMemoryUserDetailsManager;
//import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

//import com.gitlab.edersix.security.app.auth.EdersixUserDetailsService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)//turn on metod level security
public class ApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter{

//	---------------  DAO AUTHENTICATION PROVIDER
//	@Autowired
//	private EdersixUserDetailsService userDetailsService;
//	
//	@Bean 
//	public DaoAuthenticationProvider authenticationProvider() {
//		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
//		provider.setUserDetailsService(userDetailsService);
//		provider.setPasswordEncoder(new BCryptPasswordEncoder(11));//11 times to run b crypt
//		provider.setAuthoritiesMapper(authoritiesMapper());
//		return provider;
//	}	
//	------------------  DAO AUTHENTICATION PROVIDER

	
	
	
	@Bean
	public GrantedAuthoritiesMapper authoritiesMapper() {
		SimpleAuthorityMapper authoritiyMapper = new SimpleAuthorityMapper();
		authoritiyMapper.setConvertToUpperCase(true);
		authoritiyMapper.setDefaultAuthority("USER");
		return authoritiyMapper;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// TODO Auto-generated method stub
//		auth.authenticationProvider(authenticationProvider());
		
//		dn: ou=people,dc=edersix,dc=com
		auth
		.ldapAuthentication()
		.userDnPatterns("uid={0},ou=people")
		.groupSearchBase("ou=groups")
		.authoritiesMapper(authoritiesMapper())
		.contextSource()
		.url("ldap://localhost:8389/dc=edersix,dc=com")
		.and()
		.passwordCompare()
		.passwordEncoder(new BCryptPasswordEncoder(11))
		.passwordAttribute("userPassword");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO Auto-generated method stub
		http.csrf().disable()
		.authorizeRequests().antMatchers("/","/index","/css/*","/js/*").permitAll()//url's that we want allow
		.anyRequest().authenticated()//any other request require to be authenticated
		.and()
//		.httpBasic();
		.formLogin()
		.loginPage("/login").permitAll()//allow all access to login page
		.and()
		.logout().invalidateHttpSession(true)//invalidate session when logout
		.clearAuthentication(true)// clear authentication data
		.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
		.logoutSuccessUrl("/logout-success").permitAll();//where to go 
		
	}

//	@Bean
//	@Override
//	public UserDetailsService userDetailsService() {//not recommended for production 
//		// TODO Auto-generated method stub
//		List<UserDetails> users = new ArrayList<>();
//		users.add(User.withDefaultPasswordEncoder().username("eder").password("six").roles("USER","ADMIN").build());
//		users.add(User.withDefaultPasswordEncoder().username("pau").password("luna").roles("USER").build());
//		return new InMemoryUserDetailsManager(users);
//	}
	
	

}
